<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\embed;

class Landingpage extends Component
{
    public function render()
    {
        return view('livewire.landingpage',[
            'landing' => embed::all(),
        ]);
    }
}
