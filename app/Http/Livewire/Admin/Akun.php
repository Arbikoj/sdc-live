<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\User;


class Akun extends Component
{
    public $user_id, $name, $email, $password, $role;


    public function render()
    {
        return view('livewire.admin.akun',[
            'user' => User::all(),
        ]);
    }

    public function resetFields()
    {
    
        $this->user_id = '';
        $this->name = '';
        $this->email = '';
        $this->password = '';
        $this->role = '';
    }

    public function store()
    {
        //MEMBUAT VALIDASI
        // $this->validate([
            
        // ]);


        User::updateOrCreate(['id' => $this->user_id], [
    
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
            'role' => $this->role,
        ]);

        //BUAT FLASH SESSION UNTUK MENAMPILKAN ALERT NOTIFIKASI
        session()->flash('message', $this->user_id ? 'Akun Diperbaharui': 'Akun Ditambahkan');
        $this->resetFields(); //DAN BERSIHKAN FIELD
    }

    public function edit($id)
    {
        $fas = User::find($id); 
        $this->user_id = $id;

        $this->name = $fas->name;
        $this->email = $fas->email;
        $this->password = $fas->password;
        $this->role = $fas->role;

    }
}
