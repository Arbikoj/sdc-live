<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\embed;

class Dashboard extends Component
{
    
    public $user_id, $name, $email, $password, $role;

    public function render()
    {
        if (auth()->user()->hasRole('admin')) {
            return view('livewire.admin.dashboard',[
                'embed' => embed::all(),
            ]);
        } else {
            return view('livewire.user.dashboard');
        }
    }
    
}
