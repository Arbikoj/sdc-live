<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

use App\Models\embed;
use Livewire\WithPagination;

class Homepage extends Component
{
    public  $embed_id,
            $link,
            $teks1,
            $teks2,
            $teks3,
            $teks4;

    //pagination default

    public $validateDelete = 0;
    //variable search
    public $srcItem;

    public function render()
    {
        return view('livewire.admin.homepage',[
            'embed' => embed::all(),
        ]);
    }

    public function resetFields()
    {
    
        $this->embed_id = '';
        $this->link = '';
        $this->teks1 = '';
        $this->teks2 = '';
        $this->teks3 = '';
        $this->teks4 = '';
    }

    public function store()
    {
        //MEMBUAT VALIDASI
        // $this->validate([
            
        // ]);


        embed::updateOrCreate(['id' => $this->embed_id], [
    
            'link' => $this->link,
            'teks1' => $this->teks1,
            'teks2' => $this->teks2,
            'teks3' => $this->teks3,
            'teks4' => $this->teks4,

        ]);

        //BUAT FLASH SESSION UNTUK MENAMPILKAN ALERT NOTIFIKASI
        session()->flash('message', $this->embed_id ? 'Link Diperbaharui': 'Link Ditambahkan');
        $this->resetFields(); //DAN BERSIHKAN FIELD
    }

    public function edit($id)
    {
        $fas = embed::find($id); 
        $this->embed_id = $id;

        $this->link = $fas->link;
        $this->teks1 = $fas->teks1;
        $this->teks2 = $fas->teks2;
        $this->teks3 = $fas->teks3;
        $this->teks4 = $fas->teks4;

    }
}
