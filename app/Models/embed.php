<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class embed extends Model
{
    use HasFactory;
    protected $table = 'embeds';

    protected $fillable = [
        'id',
        'link',
        'teks1',
        'teks2',
        'teks3',
        'teks4',
    ];
}
