<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\Akun;
use App\Http\Livewire\Admin\Homepage;
use App\Http\Livewire\Landingpage;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('livewire.');
// });
Route::get('/', Landingpage::class)->name('landingpage');


Route::middleware(['auth:sanctum', 'verified'], ['admin'])->get('/dashboard', App\Http\Livewire\Admin\Dashboard::class)
->name('dashboard');

Route::group(['middleware' => 'auth'], function ()
{
    Route::get('/admin/homepage', Homepage::class)->name('admin.homepage');
    Route::get('/admin/akun', Akun::class)->name('admin.akun');
    // Route::get('/cart', Cart::class);
    // Route::get('/admin/gambar', FileUpload::class);


});