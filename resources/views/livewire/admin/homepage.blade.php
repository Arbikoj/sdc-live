<div class="container">
    <div class="shadow-lg my-10 rounded px-5 py-4">
        <div class="text-4xl font-bold">Setting Homepage</div>

        @if (session()->has('message'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ session('message') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif

        @foreach ($embed as $item)
        {{-- link youtube --}}
        
        <div class="row">
            <label class="text-2xl font-bold">Link youtube</label>
            <div class="col-md-6">
                @if ($id = $this->embed_id)
                    <input
                    wire:model="link"
                    class="form-control"
                    placeholder="link youtube">
                    
                @else
                    <p>{{$item->link}}</p>
                @endif
            </div>
            {{-- teks 1 --}}
            <label class="text-2xl font-bold">Teks 1</label>
            <div class="col-md-6">
                @if ($id = $this->embed_id)
                    <input
                    wire:model="teks1"
                    class="form-control"
                    placeholder="Teks 1">
                    
                @else
                    <p>{{$item->teks1}}</p>
                @endif
            </div>
            {{-- teks 2 --}}
            <label class="text-2xl font-bold">Teks 2</label>
            <div class="col-md-6">
                @if ($id = $this->embed_id)
                    <input
                    wire:model="teks2"
                    class="form-control"
                    placeholder="Teks 2">
                    
                @else
                    <p>{{$item->teks2}}</p>
                @endif
            </div>

            {{-- teks 3 --}}
            <label class="text-2xl font-bold">Teks 3</label>
            <div class="col-md-6">
                @if ($id = $this->embed_id)
                    <input
                    wire:model="teks3"
                    class="form-control"
                    placeholder="Teks 3">
                    
                @else
                    <p>{{$item->teks3}}</p>
                @endif
            </div>

            {{-- teks 4 --}}
            <label class="text-2xl font-bold">Teks 4</label>
            <div class="col-md-6">
                @if ($id = $this->embed_id)
                    <input
                    wire:model="teks4"
                    class="form-control"
                    placeholder="Teks 4">
                    
                @else
                    <p>{{$item->teks4}}</p>
                @endif
            </div>
            
            <div class="col-md">
                @if($id = $this->embed_id)
                <button wire:click="store()" type="button" class="btn btn-outline-primary m-1">Save</button>
                <button wire:click="resetFields()" type="button" class="btn btn-danger m-1">Batal</button>
                    
                @else
                    <button wire:click="edit({{ $item->id }})" type="button" class="btn btn-outline-primary">Edit</button>
                @endif
            </div>
        </div>

        
        @endforeach

    </div>
</div>
