<div class="container">
    <div class="shadow-lg my-10 rounded px-5 py-4">
        <div class="text-4xl font-bold">Manajemen Akun</div>

        @foreach ($user as $item)
        {{-- link youtube --}}
        
        <div class="row">
            <label class="text-2xl font-bold">Link youtube</label>
            <div class="col-md-6">
                @if ($id = $this->user_id)
                    <input
                    wire:model="link"
                    class="form-control"
                    placeholder="link youtube">
                    
                @else
                    <p>{{$item->link}}</p>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>