<?php

namespace Database\Seeders;

use App\Models\embed;
use Illuminate\Database\Seeder;

class EmbedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        embed::create([
            'link' => 'ini link',
            'teks1' => 'teks1',
            'teks2' => 'teks2',
            'teks3' => 'teks3',
            'teks4' => 'teks4',      
        ]);
    }
}
